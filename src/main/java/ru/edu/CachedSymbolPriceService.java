package ru.edu;

import ru.edu.model.Symbol;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CachedSymbolPriceService implements SymbolPriceService {

    private final SymbolPriceService delegate;

    private final Map<String, Symbol> cache = Collections.synchronizedMap(new HashMap<>());

    public CachedSymbolPriceService(final SymbolPriceService symbolPriceService) {
        delegate = symbolPriceService;
    }

    /**
     * Сервис по получению данных из внешнего источника.
     * Должен иметь внутренний кэш, с помощью которого данные будут обновляться не чаще, чем раз в 10 секунд.
     *
     * @param symbolName
     * @return
     */
    @Override
    public Symbol getPrice(String symbolName) {
        synchronized (symbolName) {
            if (!cache.containsKey(symbolName) || Instant.now().minus(5, ChronoUnit.SECONDS).isAfter(cache.get(symbolName).getTimeStamp())) {
                cache.put(symbolName, delegate.getPrice(symbolName));
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {

            }
        }
        return cache.get(symbolName);
    }
}
