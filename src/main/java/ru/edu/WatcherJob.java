package ru.edu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.edu.model.Symbol;

public class WatcherJob implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(WatcherJob.class);

    private final String symbolToWatch;

    private final SymbolPriceService service;

    private Symbol lastData = null;

    private int num;

    public WatcherJob(String symbol, SymbolPriceService service) {
        this.symbolToWatch = symbol;
        this.service = service;
    }

    /**
     * When an object implementing interface {@code Runnable} is used
     * to create a thread, starting the thread causes the object's
     * {@code run} method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method {@code run} is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        if(++num % 10 == 0){
            LOGGER.error("Какая-то ошибка");
            return;
        }
        Symbol symbol = service.getPrice(symbolToWatch);
        LOGGER.info("Получили ответ: {}, ранее: {}", symbol, lastData);
        lastData = symbol;
        //.flush
    }
}
