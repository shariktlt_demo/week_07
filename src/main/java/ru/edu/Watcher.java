package ru.edu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.edu.model.Symbol;

public class Watcher implements Runnable{

    private static final Logger LOGGER = LoggerFactory.getLogger(Watcher.class);

    private final String symbolToWatch;

    private final SymbolPriceService service;

    public Watcher(String symbol, SymbolPriceService service) {
        this.symbolToWatch = symbol;
        this.service = service;
    }

    /**
     * When an object implementing interface {@code Runnable} is used
     * to create a thread, starting the thread causes the object's
     * {@code run} method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method {@code run} is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        Symbol lastData= null;
        while (true){
            Symbol symbol = service.getPrice(symbolToWatch);
            LOGGER.info("Получили ответ: {}", symbol);

            try {
                Thread.sleep(1_000L);
            } catch (InterruptedException e) {

            }
        }
    }
}
