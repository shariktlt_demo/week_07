package ru.edu;

public class ManualThreads {

    public static void main(String[] args) {
        SymbolPriceService service = new BinanceSymbolPriceService();
        SymbolPriceService cached = new CachedSymbolPriceService(service);

        String[] symbols = {"BTCUSDT", "BTCUSDT", "BTCRUB"};


        Thread[] threads = new Thread[symbols.length];

        for (int i = 0; i < symbols.length; i++) {
            threads[i] = new Thread(new Watcher(symbols[i], cached));
            threads[i].start();
        }


        try {
            threads[0].join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
